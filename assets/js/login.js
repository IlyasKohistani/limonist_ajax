if (is_login()) is_valid_token();
else hideLoader();
$("#login").click(function () {
  showLoader();
  login();
});

$("#register_link").click(function () {
  window.location.href = "register.html";
});

$("#login_link").click(function () {
  window.location.href = "login.html";
});

$("#register").click(function () {
  showLoader();
  register();
});
