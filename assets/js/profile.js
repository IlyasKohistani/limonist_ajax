if (!is_login()) window.location.href = "login.html";
else hideLoader();
setInputValues(me());

$("#updateUser").click(function () {
  showLoader();
  updateMe();
});

function setInputValues(user) {
  if (!user) window.location.href = "login.html";
  $("#username").val(user.username);
  $("#name").val(user.fname);
  $("#surname").val(user.lname);
  hideLoader();
}

function updateMe() {
  showLoader();
  var username = $("#username");
  var name = $("#name");
  var surname = $("#surname");
  $.ajax({
    dataType: "json",
    type: "POST",
    async: true,
    data: {
      username: username.val(),
      name: name.val(),
      surname: surname.val(),
    },
    url: sUrl + "user",
    headers: { Authorization: "bearer " + localStorage.getItem("token") },
    success: function (data) {
      if (data.success) window.location.href = "dashboard.html";
      else {
        $.each(data.message, function (index, value) {
          alert(value);
          hideLoader();
          return;
        });
      }
    },
  });
}
