if (!is_login()) window.location.href = "login.html";

$(document).ready(function () {
  $("#logout").click(function () {
    logout();
  });
  $("#profile").click(function () {
    window.location.href = "profile.html";
  });
  $("#removeMe").click(function () {
    removeMe();
  });
  $("#dashboard_link").click(function () {
    window.location.href = "dashboard.html";
  });

  $("#datatable_link").click(function () {
    window.location.href = "datatable.html";
  });
});

function me() {
  var result = $.ajax({
    dataType: "json",
    type: "POST",
    async: false,
    url: sUrl + "auth/me",
    headers: { Authorization: "bearer " + localStorage.getItem("token") },
    success: function (data) {
      if (!data.success) return false;
    },
  });
  return result.responseJSON.data;
}

function removeMe() {
  showLoader();
  $.ajax({
    dataType: "json",
    type: "DELETE",
    async: true,
    url: sUrl + "user",
    headers: { Authorization: "bearer " + localStorage.getItem("token") },
    success: function (data) {
      if (data.success) {
        localStorage.clear();
        window.location.href = "login.html";
      } else alert(data.message);
    },
  });
}
