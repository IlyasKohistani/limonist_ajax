var sUrl = "http://127.0.0.1:8000/api/";
function setCookie(cname, cvalue, exdays = 86400000) {
  const d = new Date();
  d.setTime(d.getTime() + exdays);
  let expires = "expires=" + d.toGMTString();
  localStorage.setItem(cname, cvalue);
  localStorage.setItem("expires", d.toGMTString());
}

function getCookie(cname) {
  if (localStorage.getItem(cname)) return localStorage.getItem("lastname");
  return "";
}

function is_login() {
  let user = localStorage.getItem("token");
  if (user && user != null) {
    return true;
  }
  return false;
}

function logout() {
  showLoader();
  $.ajax({
    dataType: "json",
    type: "POST",
    async: false,
    url: sUrl + "auth/logout",
    headers: { Authorization: "bearer " + localStorage.getItem("token") },
    success: function (data) {
      if (data.success) {
        localStorage.clear();
        localStorage.removeItem("token");
        window.location.href = "login.html";
      }
      hideLoader();
      return false;
    },
    error: function (data) {
      localStorage.clear();
      localStorage.removeItem("token");
      window.location.href = "login.html";
    },
  });
}

function login() {
  var email = $("#login_email").val();
  var password = $("#login_password").val();
  $.ajax({
    dataType: "json",
    type: "POST",
    url: sUrl + "auth/login",
    data: { email: email, password: password },
    success: function (data) {
      if (data.success) {
        setCookie("token", data.access_token, data.expires_in);
        if (is_login()) window.location.href = "dashboard.html";
      } else {
        hideLoader();
        if (data.message.email) alert(data.message.email);
        else alert(data.message.password);
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      localStorage.clear();
      location.reload();
    },
  });
}

function register() {
  var email = $("#email").val();
  var password = $("#password").val();
  var password_c = $("#password_c").val();
  $.ajax({
    dataType: "json",
    type: "POST",
    url: sUrl + "auth/register",
    data: {
      email: email,
      password: password,
      password_confirmation: password_c,
    },
    success: function (data) {
      if (data.success) {
        setCookie("token", data.access_token, data.expires_in);
        if (is_login()) window.location.href = "dashboard.html";
      } else {
        hideLoader();
        if (data.message.email) alert(data.message.email);
        else alert(data.message.password);
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      localStorage.clear();
      location.reload();
    },
  });
}

function is_valid_token() {
  $.ajax({
    dataType: "json",
    type: "POST",
    url: sUrl + "auth/me",
    headers: { Authorization: "bearer " + localStorage.getItem("token") },
    success: function (data) {
      if (data.success) window.location.href = "dashboard.html";
      hideLoader();
      return false;
    },
  });
}

function hideLoader() {
  $("#loader").addClass("d-hidden");
  $("#container").removeClass("d-hidden");
}
function showLoader() {
  $("#loader").removeClass("d-hidden");
  $("#container").addClass("d-hidden");
}
